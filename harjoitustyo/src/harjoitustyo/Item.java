package harjoitustyo;

/**
 *
 * @author Jooa
 */
abstract class Item {

    protected String name;
    protected float weight; //kg
    protected float volume; //cm^3
    protected boolean fragile;

    public String getName() {
        return name;
    }

    public float getWeight() {
        return weight;
    }

    public float getVolume() {
        return volume;
    }

    public boolean isFragile() {
        return fragile;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}

class Item1 extends Item {

    Item1() {
        name = "Genelec-pari";  // specs confirmed from genelec's  site
        weight = (float) 8.0;
        volume = (float) 5188.96;
        fragile = true;
    }
}

class Item2 extends Item {

    Item2() {
        name = "Olio-ohjelmointi Javalla -opas";
        weight = (float) 0.5;
        volume = (float) 309.75;
        fragile = false;
    }
}

class Item3 extends Item {

    Item3() {
        name = "Francis Goya -Lp";
        weight = (float) 0.2;
        volume = (float) 180.0;
        fragile = true;
    }
}

class Item4 extends Item {

    Item4() {
        name = "Kilo perunoita";
        weight = (float) 1;
        volume = (float) 1800;
        fragile = false;
    }
}

class Item5 extends Item {

    Item5(String n, float w, float v, boolean b) {
        name = n;
        weight = w;
        volume = v;
        fragile = b;
    }
}
