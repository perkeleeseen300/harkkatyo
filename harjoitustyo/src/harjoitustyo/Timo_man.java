package harjoitustyo;

import java.util.Random;

/**
 *
 * @author Jooa
 */
public class Timo_man {

    private boolean badDay;

    public Timo_man() {}

    public boolean isBadDay() {
        Random random = new Random();
        badDay = random.nextBoolean();
        //Does Timo-man have a bad day?
        return badDay;
    }
}
