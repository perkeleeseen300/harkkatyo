package harjoitustyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author ATTE
 */
public class xmlLoader {

    private Document doc;

    public xmlLoader() {
    }

    public void loadXML() {
        try {
            BufferedReader br;
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            br = new BufferedReader(new InputStreamReader(url.openStream()));
            String content = "";
            String line;
            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(xmlLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

        NodeList nodes = doc.getElementsByTagName("place");
        for (int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String s0 = getValue("city", e);
            String s1 = getValue("address", e) + ", " + getValue("code", e) + " " + getValue("city", e);
            String s2 = getValue("postoffice", e);
            String s3 = getValue("availability", e);
            float f1 = Float.valueOf(getValue("lat", e));
            float f2 = Float.valueOf(getValue("lng", e));

            SmartPostLibrary.getInstance().addSmartPost(new SmartPost(s0, s1, s2, s3, f1, f2));
        }
    }

    private String getValue(String tag, Element e) {
        return (e.getElementsByTagName(tag)).item(0).getTextContent();
    }
}
