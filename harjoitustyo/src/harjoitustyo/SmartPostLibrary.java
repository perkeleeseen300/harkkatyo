package harjoitustyo;

import java.util.ArrayList;

/**
 *
 * @author ATTE
 */
public class SmartPostLibrary {

    private static final SmartPostLibrary instance = new SmartPostLibrary();
    private final ArrayList<SmartPost> spList = new ArrayList<>();

    private SmartPostLibrary() {}

    public static SmartPostLibrary getInstance() {
        return instance;
    }

    public ArrayList<SmartPost> getSpList() {
        return spList;
    }

    public void addSmartPost(SmartPost sp) {
        spList.add(sp);
    }

    public SmartPost getSP(int i) {
        return spList.get(i);
    }
}
