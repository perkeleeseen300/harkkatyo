package harjoitustyo;

import java.util.ArrayList;

/**
 *
 * @author Jooa
 */
public class Warehouse {

    private static final Warehouse instance = new Warehouse();
    private ArrayList<Package> packagelist = new ArrayList();

    private Warehouse() {}

    static public Warehouse getInstance() {
        return instance;
    }

    public void addPackage(Package p) {
        packagelist.add(p);
    }

    public Package returnPackage(Package p) {
        int i = packagelist.indexOf(p);
        return packagelist.remove(i);
        // As Itella would do, Warehouse gets the package so that it can check
        // what package they no longer have. Then they send the package back.
    }

    public ArrayList<Package> returnPackageList() {
        return packagelist;
    }
}
