package harjoitustyo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Jooa
 */
public class FXMLcreatePackageController implements Initializable {

    @FXML
    private ComboBox<String> arrivalTown;
    @FXML
    private ComboBox<SmartPost> arrivalOffice;
    @FXML
    private RadioButton firstClassRadioButton;
    @FXML
    private RadioButton secondClassRadioButton;
    @FXML
    private RadioButton thirdClassRadioButton;
    @FXML
    private Button infoButton;
    @FXML
    private ComboBox<Item> itemCombo;
    @FXML
    private TextField nameField;
    @FXML
    private TextField weightField;
    @FXML
    private CheckBox fragileBox;
    @FXML
    private ComboBox<String> departureTown;
    @FXML
    private ComboBox<SmartPost> departureOffice;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private ToggleGroup classGroup;
    @FXML
    private TextField heightField;
    @FXML
    private TextField widthField;
    @FXML
    private TextField depthField;
    @FXML
    private WebView webHelp;
    @FXML
    private Label messageLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webHelp.getEngine().load(getClass().getResource("index.html").toExternalForm());
        for (SmartPost sp : SmartPostLibrary.getInstance().getSpList()) {
            if (!arrivalTown.getItems().contains(sp.getCity())) {
                arrivalTown.getItems().add(sp.getCity());
                departureTown.getItems().add(sp.getCity());
            }
        }
        arrivalTown.getItems().sort(null);
        departureTown.getItems().sort(null);

        itemCombo.getItems().addAll(Arrays.asList(
                new Item1(), new Item2(), new Item3(), new Item4()));
        firstClassRadioButton.setSelected(true);
    }

    @FXML
    private void infoButtonAction(ActionEvent event) {  // opens info to new window
        try {
            Stage newWindow = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageInfoWindow.fxml"));
            Scene scene = new Scene(page);
            newWindow.setScene(scene);
            scene.getStylesheets().add(Harjoitustyo.class.getResource("graphics2.css").toExternalForm());
            newWindow.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void cancel(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void createPackage(ActionEvent event) {
        // Creates new package from itemCombo if it's chosen, otherwise custom item
        ParseInput pi = new ParseInput();
        Item item;
        // check that custom parameters are in correct form
        if (itemCombo.getValue() != null) {
            item = itemCombo.getValue();
        } else if (pi.isLegit(heightField.getText()) && pi.isLegit(widthField.getText())
                && pi.isLegit(depthField.getText()) && pi.isLegit(weightField.getText())) {
            float size = pi.getSize(pi.giveFloat(heightField.getText()),
                    pi.giveFloat(widthField.getText()), pi.giveFloat(depthField.getText()));
            float weight = pi.giveFloat(weightField.getText());
            item = new Item5(nameField.getText(), weight, size, fragileBox.isSelected());
        } else {
            messageLabel.setText("Tiedot syötetty väärin!");
            return;
        }

        Package p;
        if (firstClassRadioButton.isSelected()) {
            p = new firstClass(item);
        } else if (secondClassRadioButton.isSelected()) {
            p = new secondClass(item);
        } else {
            p = new thirdClass(item);
        }

        // checks that all needed information is entered and weight/range/volume limits
        if (pi.isSelected(departureOffice.getValue()) && pi.isSelected(arrivalOffice.getValue())) {
            if (item.getWeight() <= p.maxWeight) {
                if (item.volume <= p.getSize()) {
                    if (departureOffice.getValue().getPostOffice() != arrivalOffice.getValue().getPostOffice()) {
                        ArrayList<Float> al = new ArrayList<>();
                        al.addAll(Arrays.asList(
                                departureOffice.getValue().getGeoPoint().get(0), departureOffice.getValue().getGeoPoint().get(1),
                                arrivalOffice.getValue().getGeoPoint().get(0), arrivalOffice.getValue().getGeoPoint().get(1)));
                        p.setRouteArray(al);
                        
                        double routeLength = (double) webHelp.getEngine().executeScript("document.createPath("
                                + p.getRouteArray() + ", 'blue', " + p.getSpeed() + ")");
                        if (p.range >= routeLength) {
                            webHelp.getEngine().executeScript("document.deletePaths()");
                            Warehouse.getInstance().addPackage(p);
                            p.setRouteArray(al);
                            Stage stage = (Stage) createPackageButton.getScene().getWindow();
                            stage.close();
                        } else {
                            messageLabel.setText("Liian pitkä matka!");
                            webHelp.getEngine().executeScript("document.deletePaths()");
                        }
                    } else {
                        messageLabel.setText("Automaatit eivät voi olla samoja!");
                    }
                } else {
                    messageLabel.setText("Paketti on liian suuri!");
                }
            } else {
                messageLabel.setText("Paketti on liian painava!");
            }
        } else {
            messageLabel.setText("Anna pakettiautomaatit!");
        }
    }

    @FXML
    private void setComboArrivalOffice(ActionEvent event) {
        arrivalOffice.getItems().clear();
        for (SmartPost sp : SmartPostLibrary.getInstance().getSpList()) {
            if (sp.getCity().equals(arrivalTown.getValue())) {
                arrivalOffice.getItems().add(sp);
            }
        }
    }

    @FXML
    private void setComboDepartureOffice(ActionEvent event) {
        departureOffice.getItems().clear();
        for (SmartPost sp : SmartPostLibrary.getInstance().getSpList()) {
            if (sp.getCity().equals(departureTown.getValue())) {
                departureOffice.getItems().add(sp);
            }
        }
    }
}
