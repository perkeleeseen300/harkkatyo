package harjoitustyo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Jooa
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private WebView web;
    @FXML
    private ComboBox<String> smartCombo;
    @FXML
    private ComboBox<Package> packageCombo;
    @FXML
    private ListView<String> listView;
    @FXML
    private Label infoLabel;
    @FXML
    private AnchorPane mainWindow;
    @FXML
    private Label titleText;
    @FXML
    private TabPane tabPane;
    
    private Stage stage;

    // default package to prevent nullPointerExceptions in itemCombo
    private Package defaultPackage = new firstClass(new Item5("Ei luotuja paketteja", 0, 0, true));
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        xmlLoader loader = new xmlLoader();
        loader.loadXML();
        for (SmartPost sp : SmartPostLibrary.getInstance().getSpList()) {
            if (!smartCombo.getItems().contains(sp.getCity())) {
                smartCombo.getItems().add(sp.getCity());    //add SmartPost objects to combo
            }
        }
        smartCombo.getItems().sort(null);
        smartCombo.getSelectionModel().selectFirst();
        packageCombo.getItems().add(defaultPackage);
        packageCombo.getSelectionModel().selectFirst();
        logUpdater(LogRegister.getInstance().appStarted());
    }
    
    public void setStage(Stage stage) {
        this.stage = stage;
        // Getting stage from mainclass for saving the log to file before exit.
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(final WindowEvent arg0) {
                saveToFile();
            }
        });
    }
    
    @FXML
    private void addSmartPost(ActionEvent event) {      // add markers on map of the chosen city
        for (SmartPost sp : SmartPostLibrary.getInstance().getSpList()) {
            if (sp.getCity().contains(smartCombo.getValue())) {
                web.getEngine().executeScript("document.goToLocation('" + sp.getFullAddress()
                        + "', '" + sp.getPostOffice() + ", " + sp.getAvailability() + "', 'red')");
            }
        }
        logUpdater(LogRegister.getInstance().smartPostAdded(smartCombo.getValue()));
    }
    
    @FXML
    private void addPackageAction(ActionEvent event) {  // opens a new window to create new package
        infoLabel.setText("");  //reset error label
        try {
            Stage newWindow = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLcreatePackage.fxml"));
            
            Scene scene = new Scene(page);
            newWindow.setScene(scene);
            scene.getStylesheets().add(Harjoitustyo.class.getResource("graphics2.css").toExternalForm());
            newWindow.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void deletePathsAction(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");    // delete paths from map
    }
    
    @FXML
    private void reloadPackageAction(ActionEvent event) {   // update packageCombo to match wirh Warehouse
        int packageAmount, diff;
        if (packageCombo.getValue().getItem().getName().equals("Ei luotuja paketteja")) {
            packageAmount = 0;
        } else {
            packageAmount = packageCombo.getItems().size();
        }
        
        if (!Warehouse.getInstance().returnPackageList().isEmpty()) {
            packageCombo.getItems().clear();
            for (Package p : Warehouse.getInstance().returnPackageList()) {
                packageCombo.getItems().add(p);
            }
            
            packageCombo.getSelectionModel().selectFirst();
        } else {
            infoLabel.setText("Paketteja ei löytynyt.");
            return;
        }
        
        diff = packageCombo.getItems().size() - packageAmount;  // update the log for packages
        for (int i = 0; i < diff; i++) {                        //    that aren't already there
            logUpdater(LogRegister.getInstance().packageAddedToStorage(
                    packageCombo.getItems().get(packageAmount + i)));
        }
    }
    
    @FXML
    private void sendPackageAction(ActionEvent event) {     //sent the chosen package if exist
        if (!packageCombo.getValue().getItem().getName().equals("Ei luotuja paketteja")) {            
            Package p = Warehouse.getInstance().returnPackage(packageCombo.getValue());
            logUpdater(LogRegister.getInstance().packageRemovedFromStorage(p));
            double routeLength = (double) web.getEngine().executeScript("document.createPath("
                    + p.getRouteArray() + ", '" + p.getColor() + "', " + p.getSpeed() + ")");
            
            packageCombo.getItems().clear();    //update packageCombo after sending
            for (Package pp : Warehouse.getInstance().returnPackageList()) {
                packageCombo.getItems().add(pp);
            }
            
            logUpdater(LogRegister.getInstance().packageSent(p));
            logUpdater(LogRegister.getInstance().packageReceived(p, routeLength));
            
            if (packageCombo.getItems().isEmpty()) {
                packageCombo.getItems().add(defaultPackage);
                
            }
            packageCombo.getSelectionModel().selectFirst();
        } else {
            infoLabel.setText("Luo uusi paketti tai päivitä paketti");
        }
    }
    
    private void logUpdater(String s) {     // updates the listView log
        listView.getItems().add(s);
    }
    
    private void saveToFile() {     // saves the log to file before closing the window
        String filename = "smartPOST_loki.txt";
        listView.getItems().add("\nVarastoon jäi tuotteita:");
        for (Package p : Warehouse.getInstance().returnPackageList()) {
            logUpdater(LogRegister.getInstance().unSentPackages(p));
        }
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            String[] input;
            for (int i = 0; i < listView.getItems().size(); i++) {
                input = listView.getItems().get(i).split("\n");
                for (String s : input) {
                    bw.write(s);
                    bw.newLine();
                }
                bw.newLine();
            }
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void parseLog(String file) {     //used if user chooses to use old log
        ParseInput pi = new ParseInput();
        ArrayList<Float> al;
        Item item;
        Package p = null;
        String name = "null";
        float weight = 0;
        float size = 0;
        boolean fragile = false;
        
        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
            String s;
            while ((s = in.readLine()) != null) {
                if (s.indexOf(':') > -1) {         // find all parameters for Item/Package and create
                    String[] parts = s.split("\\:");
                    if ("Nimi".equals(parts[0])) {
                        name = parts[1];
                    }
                    if ("Paino".equals(parts[0])) {
                        weight = pi.giveFloat(parts[1]);
                    }
                    if ("Tilavuus".equals(parts[0])) {
                        size = pi.giveFloat(parts[1]);
                    }
                    if ("Särkyvyys".equals(parts[0])) {
                        fragile = Boolean.parseBoolean(parts[1]);
                    }
                    item = new Item5(name, weight, size, fragile);
                    
                    if ("Luokka".equals(parts[0])) {
                        if ("1".equals(parts[1])) {
                            p = new firstClass(item);
                        } else if ("2".equals(parts[1])) {
                            p = new secondClass(item);
                        } else {
                            p = new thirdClass(item);
                        }
                    }
                    if ("koordinaatit".equals(parts[0])) {
                        al = new ArrayList<>();
                        al.addAll(Arrays.asList(Float.parseFloat(parts[1]), Float.parseFloat(parts[2]),
                                Float.parseFloat(parts[3]), Float.parseFloat(parts[4])));
                        p.setRouteArray(al);
                        Warehouse.getInstance().addPackage(p);
                    }
                }
            }
            in.close();
            packageCombo.getItems().addAll(Warehouse.getInstance().returnPackageList());
            packageCombo.getSelectionModel().selectFirst();
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (Warehouse.getInstance().returnPackageList().isEmpty()) {
            infoLabel.setText("Varastosta ei löytynyt paketteja.");
        }
    }
}
