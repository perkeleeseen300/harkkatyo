package harjoitustyo;

/**
 *
 * @author Jooa
 */
public class ParseInput {

    public ParseInput() {
    }

    public boolean isLegit(String s) {
        try {
            Float.parseFloat(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isSelected(SmartPost sp) {
        return sp != null;
    }

    public float giveFloat(String s) {
        return Float.parseFloat(s);
    }

    public float getSize(float f1, float f2, float f3) {
        return f1 * f2 * f3;
    }
}
