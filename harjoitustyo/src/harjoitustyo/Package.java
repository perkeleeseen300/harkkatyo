package harjoitustyo;

import java.util.ArrayList;

/**
 *
 * @author Jooa
 */
abstract public class Package {

    protected int size; //cm^3
    protected int maxWeight; //kg
    protected int speed;
    protected int range; //km
    protected boolean broken;
    protected Item item;
    protected String routeColor;
    protected ArrayList<Float> routeArray = new ArrayList<>();
    Timo_man tm = new Timo_man();

    public int getSize() {
        return size;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public int getSpeed() {
        return speed;
    }

    public int getRange() {
        return range;
    }

    public Item getItem() {
        return item;
    }

    public ArrayList<Float> getRouteArray() {
        return routeArray;
    }

    public void setRouteArray(ArrayList<Float> routeArray) {
        this.routeArray = routeArray;
    }

    public boolean isBroken() {
        return broken;
    }

    public String getColor() {
        return routeColor;
    }

    @Override
    public String toString() {
        return this.getItem().getName();
    }
}

class firstClass extends Package {

    firstClass(Item I) {
        item = I;
        speed = 1;
        range = 150;
        size = 10000;
        maxWeight = 10;
        broken = item.isFragile(); //fragile items are automatically broken
        routeColor = "green";
    }
}

class secondClass extends Package {

    secondClass(Item I) {
        item = I;
        range = 100000000;
        speed = 2;
        size = 1000;
        maxWeight = 20;
        broken = size >= 2 * item.getVolume() && item.isFragile(); //if package is too large, fragile item will be broken
        routeColor = "blue";
    }
}

class thirdClass extends Package {

    thirdClass(Item I) {
        item = I;
        range = 100000000;  // or no range
        if (2 * item.getWeight() >= maxWeight) {
            speed = 3;
        } else {
            speed = 2;
        }
        //Speed depends on item's weight
        maxWeight = 100;
        size = 500000;
        broken = tm.isBadDay() && item.isFragile(); // if Timo-man is having a bad day and the item is fragile... well, too bad!
        routeColor = "red";
    }
}
