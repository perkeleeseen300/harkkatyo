package harjoitustyo;

import java.util.ArrayList;

/**
 *
 * @author ATTE
 */
public class SmartPost {

    private String city;
    private String fullAddress;
    private String postOffice;
    private String availability;
    private ArrayList<Float> geoPoint = null;

    public SmartPost(String city, String fullAddress, String postOffice,
            String availability, float lat, float lng) {
        this.fullAddress = fullAddress;
        this.postOffice = postOffice;
        this.availability = availability;
        this.city = city;
        geoPoint = new ArrayList<>();
        geoPoint.add(lat);
        geoPoint.add(lng);
    }

    public String getCity() {
        return city;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public String getAvailability() {
        return availability;
    }

    public ArrayList<Float> getGeoPoint() {
        return geoPoint;
    }

    @Override
    public String toString() {
        return this.getPostOffice();
    }
}
