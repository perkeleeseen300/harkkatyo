package harjoitustyo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBuilder;
import javafx.scene.control.LabelBuilder;
import javafx.scene.layout.HBoxBuilder;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author Atte Putkonen, 0418378
 * @author Jooa Pursiainen, 0401437
 * 
 */
public class Harjoitustyo extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
        Parent root = (Parent) loader.load();

        Scene scene = new Scene(root);

        stage.setScene(scene);
        scene.getStylesheets().add(Harjoitustyo.class.getResource("graphics.css").toExternalForm());
        stage.show();
        FXMLDocumentController controller = (FXMLDocumentController) loader.getController();
        // Give stage to controller to set setOnCloseRequest there
        controller.setStage(stage);

        final Stage popupWindow = new Stage();
        // demand user response on popup window before proceeding
        popupWindow.initModality(Modality.WINDOW_MODAL);
        popupWindow.initOwner(stage);
        // create simple popupwindow
        popupWindow.setScene(
                new Scene(
                        HBoxBuilder.create().styleClass("modal-dialog").children(
                        LabelBuilder.create().text("Ladataanko vanhat varastotiedot?\t\n\n").build(),
                        ButtonBuilder.create().alignment(Pos.CENTER_RIGHT).text("Lataa").onAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        controller.parseLog("smartPOST_loki.txt");
                        stage.getScene().getRoot().setEffect(null);
                        popupWindow.close();
                    }
                }).build(),
                        ButtonBuilder.create().text("Luo uusi").onAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        //System.out.println("Ei toimenpiteitä.");
                        stage.getScene().getRoot().setEffect(null);
                        popupWindow.close();
                    }
                }).build()
                ).alignment(Pos.CENTER).prefHeight(50).build()
                )
        );
        popupWindow.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
