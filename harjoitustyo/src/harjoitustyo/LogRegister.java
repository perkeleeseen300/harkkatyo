package harjoitustyo;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class LogRegister {  // creates texts for log tab in mainwindow

    private static final LogRegister instance = new LogRegister();
    private String text;

    private LogRegister() {}

    public static LogRegister getInstance() {
        return instance;
    }

    public String appStarted() {
        text = "TIMO järjestelmä käynnistetty.";
        return text;
    }

    public String packageAddedToStorage(Package p) {
        text = "Uusi " + this.getClass(p) + ". luokan '" + p.getItem().getName()
                + "' paketti lisätty varastoon.\n\n";
        return text;
    }

    public String packageSent(Package p) {
        String departureTown = "";
        String arrivalTown = "";
        for (SmartPost sp : SmartPostLibrary.getInstance().getSpList()) {
            if (sp.getGeoPoint().get(0).equals(p.getRouteArray().get(0))
                    & (sp.getGeoPoint().get(1).equals(p.getRouteArray().get(1)))) {
                departureTown = sp.getCity();
            }
            if (sp.getGeoPoint().get(0).equals(p.getRouteArray().get(2))
                    & (sp.getGeoPoint().get(1).equals(p.getRouteArray().get(3)))) {
                arrivalTown = sp.getCity();
            }
        }

        text = this.getClass(p) + ". luokan paketti '" + p.getItem().getName()
                + "' lähetetty kaupungista " + departureTown + " kaupunkiin "
                + arrivalTown + ".\n\n";
        return text;
    }

    public String packageReceived(Package p, double routeLength) {
        String departureInfo = "";
        String arrivalInfo = "";
        String isBroken = "ehjänä";
        if (p.isBroken()) {
            isBroken = "rikkinäisenä";
        }

        for (SmartPost sp : SmartPostLibrary.getInstance().getSpList()) {
            if (sp.getGeoPoint().get(0).equals(p.getRouteArray().get(0))
                    & (sp.getGeoPoint().get(1).equals(p.getRouteArray().get(1)))) {
                departureInfo = sp.getPostOffice() + ", " + sp.getFullAddress();
            }
            if (sp.getGeoPoint().get(0).equals(p.getRouteArray().get(2))
                    & (sp.getGeoPoint().get(1).equals(p.getRouteArray().get(3)))) {
                arrivalInfo = sp.getPostOffice() + ", " + sp.getFullAddress();
            }
        }

        text = this.getClass(p) + ". luokan paketti saapunut määrän päähän "
                + isBroken + ". Kuljettu matka (km): " + routeLength + " \nLähtöpaikka: "
                + departureInfo + "\nSaapumispaikka: " + arrivalInfo + "\nTuote: "
                + p.getItem().getName() + "\nPaino (kg): " + p.getItem().getWeight()
                + " \nTilavuus (cm^3): " + p.getItem().getVolume() + "\n\n";
        return text;
    }

    public String packageRemovedFromStorage(Package p) {
        text = this.getClass(p) + ". luokan paketti poistunut varastosta.\n\n";
        return text;
    }

    public String unSentPackages(Package p) {
        text = "Nimi:" + p.getItem().getName()
                + "\nPaino:" + p.getItem().getWeight()
                + "\nTilavuus:" + p.getItem().getVolume()
                + "\nSärkyvyys:" + p.getItem().isFragile()
                + "\nLuokka:" + this.getClass(p)
                + "\nkoordinaatit:"
                + p.getRouteArray().get(0) + ":" + p.getRouteArray().get(1) + ":"
                + p.getRouteArray().get(2) + ":" + p.getRouteArray().get(3);
        return text;
    }

    public String smartPostAdded(String city) {
        text = "Kaupungin " + city + " smart postit lisätty kartalle.\n";
        return text;
    }

    private int getClass(Package p) {
        int packageClass;
        if (p.getClass().getCanonicalName().contains("firstClass")) {
            packageClass = 1;
        } else if (p.getClass().getCanonicalName().contains("secondClass")) {
            packageClass = 2;
        } else {
            packageClass = 3;
        }
        return packageClass;
    }
}
