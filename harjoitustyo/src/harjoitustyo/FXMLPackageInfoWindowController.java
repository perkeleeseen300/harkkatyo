package harjoitustyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Jooa
 */
public class FXMLPackageInfoWindowController implements Initializable {

    @FXML
    private Label TextLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Item item = new Item1();
        Package fc = new firstClass(item);
        Package sc = new secondClass(item);
        Package tc = new thirdClass(item);
        String text = "Tietoa pakettiluokista:\n"
                + "1. Luokka - Nopein, mutta voi kuljettaa vain 150 km päähän.\n"
                + "\t\t  Max. tilavuus: " + fc.size + " cm^3\n"
                + "\t\t  Max. paino: " + fc.maxWeight + "kg\n"
                + "\t\t  Ei särkyvää!\n"
                + "2. Luokka - Hitaampi kuin 1. Luokka, mutta voi kuljettaa koko Suomeen.\n"
                + "\t\t  Max. tilavuus: " + sc.size + " cm^3\n"
                + "\t\t  Max. paino: " + sc.maxWeight + "kg\n"
                + "\t\t  Liian pienet särkyvät tuotteet eivät kestä kuljetusta!\n"
                + "3. Luokka - Nopeus riippuu tavaran painosta, voi kuljettaa koko Suomeen.\n"
                + "\t\t  Max. tilavuus: " + tc.size + " cm^3\n"
                + "\t\t  Max. paino: " + tc.maxWeight + "kg\n"
                + "\t\t  Särkyvät tuotteet saattavat hajota";
        TextLabel.setText(text);
    }
}
